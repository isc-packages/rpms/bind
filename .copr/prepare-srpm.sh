#!/bin/bash

set -e
set -v

if [[ -z "${3}" ]]; then
	echo "Usage: ${0} source_url package_revision output_dir"
	exit 1
fi

GIT_SOURCES_DIR="$(dirname "$(dirname "$(readlink -e "${0}")")")"
RPMBUILD_SOURCES_DIR="$(rpm --eval "%{_sourcedir}")"
SOURCE_TARBALL="${RPMBUILD_SOURCES_DIR}/${1##*/}"

# If dnf is available, ensure the utilities required to build the SRPM are installed.
# Otherwise, assume these utilities are already installed in the build environment.
if command -v dnf > /dev/null 2>&1; then
	dnf -y install gnupg2 scl-utils-build
fi

# Initialize a GnuPG database that only contains the current ISC code signing keys.
GNUPGHOME="$(mktemp -d)"
export GNUPGHOME
gpg2 --import "${GIT_SOURCES_DIR}"/isc-keyblock.asc

# Prepare contents of SOURCES directory.
cp --verbose "${GIT_SOURCES_DIR}"/* "${RPMBUILD_SOURCES_DIR}"
curl --output "${SOURCE_TARBALL}" --verbose "${1}"

# Download and verify source tarball signature.
curl --output "${SOURCE_TARBALL}.asc" --verbose "${1}.asc"
gpg2 --verify "${SOURCE_TARBALL}.asc" "${SOURCE_TARBALL}"

# Set up *.spec variables based on "configure.ac".
eval "$(tar --wildcards --extract --to-stdout --file "${SOURCE_TARBALL}" "*/configure.ac" | sed -nE "s|m4_define\\(\\[bind_(VERSION_.*?)\\], (.*?)\\)dnl$|\\1=\\2|p")"
UPSTREAM_VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}${VERSION_EXTRA}"
PACKAGE_VERSION="${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}"
if [[ "${VERSION_EXTRA}" =~ ^- ]]; then
	# -P etc.
	PACKAGE_VERSION+="${VERSION_EXTRA//-/.}"
	PACKAGE_RELEASE_BASE="1"
elif [[ -n "${VERSION_EXTRA}" ]]; then
	# alpha, beta, RC, etc.
	PACKAGE_RELEASE_BASE="0.${VERSION_EXTRA}"
else
	# Regular releases.
	PACKAGE_RELEASE_BASE="1"
fi

# Put variables and the rest of the *.spec file together.
cat <<EOF > bind.spec
%global TARBALL_FORMAT ${SOURCE_TARBALL##*.}
%global UPSTREAM_VERSION ${UPSTREAM_VERSION}
%global PACKAGE_VERSION ${PACKAGE_VERSION}
%global PACKAGE_RELEASE ${PACKAGE_RELEASE_BASE}.${2}
%global MAJOR_VERSION ${VERSION_MAJOR}
%global MINOR_VERSION ${VERSION_MINOR}
%global PATCH_VERSION ${VERSION_PATCH}
EOF
cat "${GIT_SOURCES_DIR}"/bind.spec.in >> bind.spec

# Build SRPM and move it to where Copr expects it.
rpmbuild -bs bind.spec
cd "${RPMBUILD_SOURCES_DIR}/../SRPMS"
chown --verbose --reference="${3}" ./*.src.rpm
mv --verbose ./*.src.rpm "${3}"
